resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'


---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/gsxr1000/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/gsxr1000/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/gsxr1000/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/gsxr1000/carvariations.meta'

files {
'data/gsxr1000/handling.meta',
'data/gsxr1000/vehicles.meta',
'data/gsxr1000/carcols.meta',
'data/gsxr1000/carvariations.meta',
}


---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/r12014/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/r12014/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/r12014/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/r12014/carvariations.meta'

files {
'data/r12014/handling.meta',
'data/r12014/vehicles.meta',
'data/r12014/carcols.meta',
'data/r12014/carvariations.meta',
}

---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/z1000/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/z1000/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/z1000/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/z1000/carvariations.meta'

files {
'data/z1000/handling.meta',
'data/z1000/vehicles.meta',
'data/z1000/carcols.meta',
'data/z1000/carvariations.meta',
}

---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/augustaf4/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/augustaf4/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/augustaf4/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/augustaf4/carvariations.meta'

files {
'data/augustaf4/handling.meta',
'data/augustaf4/vehicles.meta',
'data/augustaf4/carcols.meta',
'data/augustaf4/carvariations.meta',
}


---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/fz6/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/fz6/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/fz6/carvariations.meta'

files {
'data/fz6/handling.meta',
'data/fz6/vehicles.meta',
'data/fz6/carvariations.meta',
}





---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/xtprf/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/xtprf/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/xtprf/carvariations.meta'

files {
'data/xtprf/handling.meta',
'data/xtprf/vehicles.meta',
'data/xtprf/carvariations.meta',
}





---------------------------------------------------------------------



data_file 'HANDLING_FILE' 'data/hondabiz/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/hondabiz/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/hondabiz/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/hondabiz/carvariations.meta'

files {
'data/hondabiz/handling.meta',
'data/hondabiz/vehicles.meta',
'data/hondabiz/carcols.meta',
'data/hondabiz/carvariations.meta',
}

---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/ktmexctrr/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/ktmexctrr/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/ktmexctrr/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/ktmexctrr/carvariations.meta'

files {
'data/ktmexctrr/handling.meta',
'data/ktmexctrr/vehicles.meta',
'data/ktmexctrr/carcols.meta',
'data/ktmexctrr/carvariations.meta'
}

client_script 'vehicle_names.lua'